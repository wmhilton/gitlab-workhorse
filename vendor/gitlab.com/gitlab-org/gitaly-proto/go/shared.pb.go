// Code generated by protoc-gen-go. DO NOT EDIT.
// source: shared.proto

package gitaly

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import google_protobuf "github.com/golang/protobuf/ptypes/timestamp"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type Repository struct {
	StorageName  string `protobuf:"bytes,2,opt,name=storage_name,json=storageName" json:"storage_name,omitempty"`
	RelativePath string `protobuf:"bytes,3,opt,name=relative_path,json=relativePath" json:"relative_path,omitempty"`
	// Sets the GIT_OBJECT_DIRECTORY envvar on git commands to the value of this field.
	// It influences the object storage directory the SHA1 directories are created underneath.
	GitObjectDirectory string `protobuf:"bytes,4,opt,name=git_object_directory,json=gitObjectDirectory" json:"git_object_directory,omitempty"`
	// Sets the GIT_ALTERNATE_OBJECT_DIRECTORIES envvar on git commands to the values of this field.
	// It influences the list of Git object directories which can be used to search for Git objects.
	GitAlternateObjectDirectories []string `protobuf:"bytes,5,rep,name=git_alternate_object_directories,json=gitAlternateObjectDirectories" json:"git_alternate_object_directories,omitempty"`
	// Used in callbacks to GitLab so that it knows what repository the event is
	// associated with. May be left empty on RPC's that do not perform callbacks.
	GlRepository string `protobuf:"bytes,6,opt,name=gl_repository,json=glRepository" json:"gl_repository,omitempty"`
}

func (m *Repository) Reset()                    { *m = Repository{} }
func (m *Repository) String() string            { return proto.CompactTextString(m) }
func (*Repository) ProtoMessage()               {}
func (*Repository) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{0} }

func (m *Repository) GetStorageName() string {
	if m != nil {
		return m.StorageName
	}
	return ""
}

func (m *Repository) GetRelativePath() string {
	if m != nil {
		return m.RelativePath
	}
	return ""
}

func (m *Repository) GetGitObjectDirectory() string {
	if m != nil {
		return m.GitObjectDirectory
	}
	return ""
}

func (m *Repository) GetGitAlternateObjectDirectories() []string {
	if m != nil {
		return m.GitAlternateObjectDirectories
	}
	return nil
}

func (m *Repository) GetGlRepository() string {
	if m != nil {
		return m.GlRepository
	}
	return ""
}

// Corresponds to Gitlab::Git::Commit
type GitCommit struct {
	Id        string        `protobuf:"bytes,1,opt,name=id" json:"id,omitempty"`
	Subject   []byte        `protobuf:"bytes,2,opt,name=subject,proto3" json:"subject,omitempty"`
	Body      []byte        `protobuf:"bytes,3,opt,name=body,proto3" json:"body,omitempty"`
	Author    *CommitAuthor `protobuf:"bytes,4,opt,name=author" json:"author,omitempty"`
	Committer *CommitAuthor `protobuf:"bytes,5,opt,name=committer" json:"committer,omitempty"`
	ParentIds []string      `protobuf:"bytes,6,rep,name=parent_ids,json=parentIds" json:"parent_ids,omitempty"`
}

func (m *GitCommit) Reset()                    { *m = GitCommit{} }
func (m *GitCommit) String() string            { return proto.CompactTextString(m) }
func (*GitCommit) ProtoMessage()               {}
func (*GitCommit) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{1} }

func (m *GitCommit) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *GitCommit) GetSubject() []byte {
	if m != nil {
		return m.Subject
	}
	return nil
}

func (m *GitCommit) GetBody() []byte {
	if m != nil {
		return m.Body
	}
	return nil
}

func (m *GitCommit) GetAuthor() *CommitAuthor {
	if m != nil {
		return m.Author
	}
	return nil
}

func (m *GitCommit) GetCommitter() *CommitAuthor {
	if m != nil {
		return m.Committer
	}
	return nil
}

func (m *GitCommit) GetParentIds() []string {
	if m != nil {
		return m.ParentIds
	}
	return nil
}

type CommitAuthor struct {
	Name  []byte                     `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Email []byte                     `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Date  *google_protobuf.Timestamp `protobuf:"bytes,3,opt,name=date" json:"date,omitempty"`
}

func (m *CommitAuthor) Reset()                    { *m = CommitAuthor{} }
func (m *CommitAuthor) String() string            { return proto.CompactTextString(m) }
func (*CommitAuthor) ProtoMessage()               {}
func (*CommitAuthor) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{2} }

func (m *CommitAuthor) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *CommitAuthor) GetEmail() []byte {
	if m != nil {
		return m.Email
	}
	return nil
}

func (m *CommitAuthor) GetDate() *google_protobuf.Timestamp {
	if m != nil {
		return m.Date
	}
	return nil
}

type ExitStatus struct {
	Value int32 `protobuf:"varint,1,opt,name=value" json:"value,omitempty"`
}

func (m *ExitStatus) Reset()                    { *m = ExitStatus{} }
func (m *ExitStatus) String() string            { return proto.CompactTextString(m) }
func (*ExitStatus) ProtoMessage()               {}
func (*ExitStatus) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{3} }

func (m *ExitStatus) GetValue() int32 {
	if m != nil {
		return m.Value
	}
	return 0
}

// Corresponds to Gitlab::Git::Branch
type Branch struct {
	Name         []byte     `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	TargetCommit *GitCommit `protobuf:"bytes,2,opt,name=target_commit,json=targetCommit" json:"target_commit,omitempty"`
}

func (m *Branch) Reset()                    { *m = Branch{} }
func (m *Branch) String() string            { return proto.CompactTextString(m) }
func (*Branch) ProtoMessage()               {}
func (*Branch) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{4} }

func (m *Branch) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *Branch) GetTargetCommit() *GitCommit {
	if m != nil {
		return m.TargetCommit
	}
	return nil
}

type Tag struct {
	Name         []byte     `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Id           string     `protobuf:"bytes,2,opt,name=id" json:"id,omitempty"`
	TargetCommit *GitCommit `protobuf:"bytes,3,opt,name=target_commit,json=targetCommit" json:"target_commit,omitempty"`
	Message      []byte     `protobuf:"bytes,4,opt,name=message,proto3" json:"message,omitempty"`
}

func (m *Tag) Reset()                    { *m = Tag{} }
func (m *Tag) String() string            { return proto.CompactTextString(m) }
func (*Tag) ProtoMessage()               {}
func (*Tag) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{5} }

func (m *Tag) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *Tag) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Tag) GetTargetCommit() *GitCommit {
	if m != nil {
		return m.TargetCommit
	}
	return nil
}

func (m *Tag) GetMessage() []byte {
	if m != nil {
		return m.Message
	}
	return nil
}

type User struct {
	GlId  string `protobuf:"bytes,1,opt,name=gl_id,json=glId" json:"gl_id,omitempty"`
	Name  []byte `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Email []byte `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"`
}

func (m *User) Reset()                    { *m = User{} }
func (m *User) String() string            { return proto.CompactTextString(m) }
func (*User) ProtoMessage()               {}
func (*User) Descriptor() ([]byte, []int) { return fileDescriptor9, []int{6} }

func (m *User) GetGlId() string {
	if m != nil {
		return m.GlId
	}
	return ""
}

func (m *User) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *User) GetEmail() []byte {
	if m != nil {
		return m.Email
	}
	return nil
}

func init() {
	proto.RegisterType((*Repository)(nil), "gitaly.Repository")
	proto.RegisterType((*GitCommit)(nil), "gitaly.GitCommit")
	proto.RegisterType((*CommitAuthor)(nil), "gitaly.CommitAuthor")
	proto.RegisterType((*ExitStatus)(nil), "gitaly.ExitStatus")
	proto.RegisterType((*Branch)(nil), "gitaly.Branch")
	proto.RegisterType((*Tag)(nil), "gitaly.Tag")
	proto.RegisterType((*User)(nil), "gitaly.User")
}

func init() { proto.RegisterFile("shared.proto", fileDescriptor9) }

var fileDescriptor9 = []byte{
	// 500 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x52, 0xc1, 0x6e, 0xd3, 0x40,
	0x10, 0x55, 0x1c, 0xc7, 0x90, 0x89, 0x8b, 0x60, 0xe9, 0xc1, 0xaa, 0x54, 0x11, 0xcc, 0xa5, 0x07,
	0xe4, 0xa2, 0x20, 0x71, 0x2f, 0x50, 0x55, 0xe5, 0x00, 0x68, 0x09, 0x67, 0x6b, 0x12, 0x0f, 0xeb,
	0x45, 0x76, 0x36, 0xda, 0x9d, 0x54, 0x44, 0x5c, 0xf8, 0x3e, 0xbe, 0x0a, 0x79, 0x37, 0x4e, 0x0b,
	0x44, 0x88, 0xdb, 0xce, 0xec, 0x9b, 0x99, 0xf7, 0xe6, 0x0d, 0xa4, 0xae, 0x46, 0x4b, 0x55, 0xb1,
	0xb6, 0x86, 0x8d, 0x48, 0x94, 0x66, 0x6c, 0xb6, 0x27, 0x4f, 0x94, 0x31, 0xaa, 0xa1, 0x73, 0x9f,
	0x5d, 0x6c, 0xbe, 0x9c, 0xb3, 0x6e, 0xc9, 0x31, 0xb6, 0xeb, 0x00, 0xcc, 0x7f, 0x44, 0x00, 0x92,
	0xd6, 0xc6, 0x69, 0x36, 0x76, 0x2b, 0x9e, 0x42, 0xea, 0xd8, 0x58, 0x54, 0x54, 0xae, 0xb0, 0xa5,
	0x2c, 0x9a, 0x0e, 0xce, 0xc6, 0x72, 0xb2, 0xcb, 0xbd, 0xc7, 0x96, 0xc4, 0x33, 0x38, 0xb2, 0xd4,
	0x20, 0xeb, 0x1b, 0x2a, 0xd7, 0xc8, 0x75, 0x36, 0xf4, 0x98, 0xb4, 0x4f, 0x7e, 0x44, 0xae, 0xc5,
	0x0b, 0x38, 0x56, 0x9a, 0x4b, 0xb3, 0xf8, 0x4a, 0x4b, 0x2e, 0x2b, 0x6d, 0x69, 0xd9, 0xf5, 0xcf,
	0x62, 0x8f, 0x15, 0x4a, 0xf3, 0x07, 0xff, 0xf5, 0xb6, 0xff, 0x11, 0x57, 0x30, 0xed, 0x2a, 0xb0,
	0x61, 0xb2, 0x2b, 0x64, 0xfa, 0xb3, 0x56, 0x93, 0xcb, 0x46, 0xd3, 0xe1, 0xd9, 0x58, 0x9e, 0x2a,
	0xcd, 0x17, 0x3d, 0xec, 0xf7, 0x36, 0x9a, 0x5c, 0xc7, 0x4f, 0x35, 0xa5, 0xdd, 0x6b, 0xca, 0x92,
	0xc0, 0x4f, 0x35, 0xb7, 0x3a, 0xdf, 0xc5, 0xf7, 0x07, 0x0f, 0x23, 0x19, 0x77, 0xfc, 0xf3, 0x9f,
	0x03, 0x18, 0x5f, 0x69, 0x7e, 0x63, 0xda, 0x56, 0xb3, 0x78, 0x00, 0x91, 0xae, 0xb2, 0x81, 0xaf,
	0x89, 0x74, 0x25, 0x32, 0xb8, 0xe7, 0x36, 0x7e, 0x88, 0x5f, 0x46, 0x2a, 0xfb, 0x50, 0x08, 0x88,
	0x17, 0xa6, 0xda, 0x7a, 0xfd, 0xa9, 0xf4, 0x6f, 0xf1, 0x1c, 0x12, 0xdc, 0x70, 0x6d, 0xac, 0x57,
	0x3a, 0x99, 0x1d, 0x17, 0xc1, 0x88, 0x22, 0x74, 0xbf, 0xf0, 0x7f, 0x72, 0x87, 0x11, 0x33, 0x18,
	0x2f, 0x7d, 0x9e, 0xc9, 0x66, 0xa3, 0x7f, 0x14, 0xdc, 0xc2, 0xc4, 0x29, 0xc0, 0x1a, 0x2d, 0xad,
	0xb8, 0xd4, 0x95, 0xcb, 0x12, 0xbf, 0x91, 0x71, 0xc8, 0x5c, 0x57, 0x2e, 0xaf, 0x21, 0xbd, 0x5b,
	0xd9, 0x91, 0xf4, 0x46, 0x0e, 0x02, 0xc9, 0xee, 0x2d, 0x8e, 0x61, 0x44, 0x2d, 0xea, 0x66, 0x27,
	0x28, 0x04, 0xa2, 0x80, 0xb8, 0x42, 0x26, 0x2f, 0x67, 0x32, 0x3b, 0x29, 0xc2, 0xe5, 0x14, 0xfd,
	0xe5, 0x14, 0xf3, 0xfe, 0x72, 0xa4, 0xc7, 0xe5, 0x39, 0xc0, 0xe5, 0x37, 0xcd, 0x9f, 0x18, 0x79,
	0xe3, 0xba, 0x9e, 0x37, 0xd8, 0x6c, 0xc2, 0xa0, 0x91, 0x0c, 0x41, 0x3e, 0x87, 0xe4, 0xb5, 0xc5,
	0xd5, 0xb2, 0x3e, 0xc8, 0xe3, 0x15, 0x1c, 0x31, 0x5a, 0x45, 0x5c, 0x06, 0x79, 0x9e, 0xcf, 0x64,
	0xf6, 0xa8, 0x5f, 0xc1, 0xde, 0x14, 0x99, 0x06, 0x5c, 0x88, 0xf2, 0xef, 0x30, 0x9c, 0xa3, 0x3a,
	0xd8, 0x32, 0xb8, 0x17, 0xed, 0xdd, 0xfb, 0x6b, 0xc4, 0xf0, 0xbf, 0x46, 0x74, 0xae, 0xb7, 0xe4,
	0x1c, 0x2a, 0xf2, 0x46, 0xa6, 0xb2, 0x0f, 0xf3, 0x4b, 0x88, 0x3f, 0x3b, 0xb2, 0xe2, 0x31, 0x8c,
	0x54, 0x53, 0xee, 0x4f, 0x25, 0x56, 0xcd, 0x75, 0xb5, 0xa7, 0x14, 0x1d, 0xda, 0xf6, 0xf0, 0xce,
	0xb6, 0x17, 0x89, 0xdf, 0xeb, 0xcb, 0x5f, 0x01, 0x00, 0x00, 0xff, 0xff, 0xaa, 0x7f, 0xf5, 0xea,
	0xb7, 0x03, 0x00, 0x00,
}
